#! /bin/bash

# Keynote 
# The script should automatically update a new server, 
# install LAMP and 
# install the php sysinfo app, all in one execution.

#Updating server and installing apache2
sudo apt update
sudo apt install apache2 -y
sudo ufw app info "Apache Full"
sudo ufw allow in "Apache Full"
sudo apt install curl

#installing MYSQL
sudo apt update
sudo apt install mysql-server -y

#installing PHP 
sudo apt update
sudo apt install php libapache2-mod-php php-mysql -y

sudo systemctl restart apache2

sudo systemctl status apache2


#Installing php sysinfo application
cd .. && mkdir sysinfo 
cd sysinfo
git clone https://github.com/phpsysinfo/phpsysinfo.git && cd /home/kasalian